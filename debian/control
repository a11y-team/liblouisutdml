Source: liblouisutdml
Priority: optional
Maintainer: Debian Accessibility Team <pkg-a11y-devel@alioth-lists.debian.net>
Uploaders:
 Paul Gevers <elbrus@debian.org>,
 Samuel Thibault <sthibault@debian.org>,
Build-Depends: dpkg-dev (>= 1.22.5),
 ant [!hppa !hurd-any !kfreebsd-any],
 automake, autoconf-archive,
 chrpath,
 debhelper-compat (= 13),
 default-jdk [!hppa !hurd-any],
 help2man,
 java-common,
 liblouis-dev (>= 3.30~), liblouis-dev:native (>= 3.13),
 libxml2-dev, libxml2-dev:native,
 libtool,
 pkgconf,
 texinfo
Standards-Version: 4.6.1
Section: libs
Vcs-Git: https://salsa.debian.org/a11y-team/liblouisutdml.git
Vcs-Browser: https://salsa.debian.org/a11y-team/liblouisutdml
Homepage: http://liblouis.org/
Rules-Requires-Root: no

Package: liblouisutdml-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 liblouisutdml9t64 (= ${binary:Version}), liblouis-dev,
 ${misc:Depends},
Description: Braille UTDML translation library - static libs and headers
 liblouisutdml is an extension of liblouisxml, the translator from xml
 format into transcribed braille, that includes support for Unified
 Tactile Document Markup Language (UTDML).
 .
 This consists essentially of <brl> subtrees containing all the braille
 translations and formatting, plus some <meta> tags.
 .
 This package contains static libraries and development headers.

Package: liblouisutdml9t64
Provides: ${t64:Provides}
Replaces: liblouisutdml9
Breaks: liblouisutdml9 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends:
 liblouisutdml-data,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Braille UTDML translation library - shared libs
 liblouisutdml is an extension of liblouisxml, the translator from xml
 format into transcribed braille, that includes support for Unified
 Tactile Document Markup Language (UTDML).
 .
 This consists essentially of <brl> subtrees containing all the braille
 translations and formatting, plus some <meta> tags.
 .
 This package contains shared libraries.

Package: liblouisutdml-java
Section: java
Architecture: all
Multi-Arch: foreign
Depends:
 liblouisutdml9t64,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Braille UTDML translation library - java bindings
 liblouisutdml is an extension of liblouisxml, the translator from xml
 format into transcribed braille, that includes support for Unified
 Tactile Document Markup Language (UTDML).
 .
 This consists essentially of <brl> subtrees containing all the braille
 translations and formatting, plus some <meta> tags.
 .
 This package contains java bindings.

Package: liblouisutdml-java-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Braille UTDML translation library - java bindings documentation
 liblouisutdml is an extension of liblouisxml, the translator from xml
 format into transcribed braille, that includes support for Unified
 Tactile Document Markup Language (UTDML).
 .
 This consists essentially of <brl> subtrees containing all the braille
 translations and formatting, plus some <meta> tags.
 .
 This package contains documentation for the java bindings.

Package: liblouisutdml-data
Section: text
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Braille UTDML translation library - data
 liblouisutdml is an extension of liblouisxml, the translator from xml
 format into transcribed braille, that includes support for Unified
 Tactile Document Markup Language (UTDML).
 .
 This consists essentially of <brl> subtrees containing all the braille
 translations and formatting, plus some <meta> tags.
 .
 This package contains runtime data.

Package: liblouisutdml-bin
Section: text
Multi-Arch: foreign
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 antiword,
 ooo2dbk,
 poppler-utils,
 rtf2xml,
Description: Braille UTDML translation utilities
 liblouisutdml is an extension of liblouisxml, the translator from xml
 format into transcribed braille, that includes support for Unified
 Tactile Document Markup Language (UTDML).
 .
 This consists essentially of <brl> subtrees containing all the braille
 translations and formatting, plus some <meta> tags.
 .
 This package contains utdml2brl which translates an utdml or text file into an
 embosser-ready braille file.
